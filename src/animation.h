class Animation {
public:
	virtual void Init(t_nbBus*)=0;
	virtual void Start()=0;
	virtual bool Complete()=0;
	virtual void End()=0;
	virtual bool Update(unsigned long time)=0;
	virtual ~Animation() {
	}
};
struct sDot {
	short addr;
	RgbColor rgb;
};

//Анимация попадания
class HitAnimation: public Animation {
private:
	t_nbBus* strip;
	unsigned long startTime;
	unsigned short leds;
	unsigned short numtDots = 1; //Активных центров каждого типа
	unsigned short  numDots; //Всего активных центров
	sDot *dots; //Горячие точки+холодные точки+2 крайних
public:
	static const uint8_t ANIM_NUM=0;

	RgbColor _getRgb(bool hot);
	virtual void Init(t_nbBus *b);
	virtual void Start();
	virtual void End();
	virtual bool Update(unsigned long time);
	virtual bool Complete();
	virtual ~HitAnimation() {

	}
};

//Анимация когда все спокойно
class IdleAnimation:public Animation {
public:
	static const uint8_t ANIM_NUM=1;
	t_nbBus* strip;
	virtual void Init(t_nbBus* b);
	virtual void Start();
	virtual void End();
	virtual bool Complete();
	virtual bool Update(unsigned long time);
};

class BleedingAnimation:public Animation {
public:

	static const uint8_t ANIM_NUM=2;
	bool bEven;
	unsigned long lastUpdate=0;
	t_nbBus* strip;
	virtual void Init(t_nbBus* b);
	virtual void Start();
	virtual void End();
	virtual bool Complete();
	virtual bool Update(unsigned long time);
};
class WoundedAnimation:public Animation {
public:

	static const uint8_t ANIM_NUM=3;
	int last_sheld_state=-1;
	t_nbBus* strip;
	virtual void Init(t_nbBus* b);
	virtual void Start();
	virtual void End();
	virtual bool Complete();
	virtual bool Update(unsigned long time);
};
class DeadAnimation:public Animation {
public:
	static const uint8_t ANIM_NUM=4;
	bool bEven;
	unsigned long lastUpdate=0;
	t_nbBus* strip;
	virtual void Init(t_nbBus* b);
	virtual void Start();
	virtual void End();
	virtual bool Complete();
	virtual bool Update(unsigned long time);
};

class RechargeAnimation:public Animation {
public:
	static const uint8_t ANIM_NUM=5;
	t_nbBus* strip=0;
	unsigned short leds=0,now=0;
	unsigned long lastUpdate=0;

	virtual void Init(t_nbBus* b);
	virtual void Start();
	virtual void End();
	virtual bool Complete();
	virtual bool Update(unsigned long time);
};

class ChargeAnimation:public Animation {
public:
	static const uint8_t ANIM_NUM=6;
	t_nbBus* strip=0;
	unsigned short leds=0;
	unsigned long lastUpdate=0;
	bool even = false;
	virtual void Init(t_nbBus* b);
	virtual void Start();
	virtual void End();
	virtual bool Complete();
	virtual bool Update(unsigned long time);
};
class StandbyAnimation:public Animation {
public:
	static const uint8_t ANIM_NUM=7;
	t_nbBus* strip=0;
	unsigned short leds=0;
	unsigned long lastUpdate=0;

	virtual void Init(t_nbBus* b);
	virtual void Start();
	virtual void End();
	virtual bool Complete();
	virtual bool Update(unsigned long time);
};