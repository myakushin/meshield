#ifndef SHIELD_STRIP_H
#define SHIELD_STRIP_H
//Логика работы ленты


class LedStrip {
public:
	unsigned short leds;
	unsigned short pin;

	t_nbBus strip;
	Animation* anims[ANIMS_MAX+1];
	Animation* paNow = 0;
	LedStrip(unsigned short l, unsigned short p);
	void Start();
	void SetAnimation(uint8_t n);
	bool update();
};

extern LedStrip* strip;

#endif