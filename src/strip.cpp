#include <Arduino.h>
#include "config.h"
#include "debug.h"
#include "animation.h"
#include "strip.h"

LedStrip* strip;
//LedStrip
LedStrip::LedStrip(unsigned short l, unsigned short p) :
		strip(l, p) {
	leds = l;
	pin = p;
    
#define NEWANIMATION(anim) anims[anim::ANIM_NUM]=new anim();
	NEWANIMATION(HitAnimation);
	NEWANIMATION(IdleAnimation);
	NEWANIMATION(RechargeAnimation);
	NEWANIMATION(BleedingAnimation);
	NEWANIMATION(WoundedAnimation);
	NEWANIMATION(DeadAnimation);
	NEWANIMATION(ChargeAnimation);
	NEWANIMATION(StandbyAnimation);
}
void LedStrip::Start() {
	debugline();
	strip.Begin();
	strip.ClearTo(RgbColor(0, 0, 0));
	strip.Show();
	debugline();
	for (int i = 0; i < ANIMS_MAX; i++)
	{
		debugline();
		anims[i]->Init(&strip);
	}
	debug("Led strip on pin "+String(pin)+" with leds "+String(leds)+" inited\n");
}
void LedStrip::SetAnimation(uint8_t n) {
	if (paNow != 0) {
		paNow->End();
	}
	if (n >= ANIMS_MAX)
	{
		debug("ERROR: Unnkon animation type ");
		debugln(n);
		return;
	}
	debug("Set animation to ");
	debugln(n);
	paNow = anims[n];
	paNow->Start();
}
bool LedStrip::update() {
	//strip.SetPixelColor(s++,RgbColor(0,0,255));
	//strip.Show();
	bool bUpdated = false;
	if (paNow != 0) {
		bUpdated = paNow->Update(0);
	}
	if (bUpdated)
		strip.Show();

	return bUpdated;
}

