#include <Arduino.h>
#include <SSD1306.h>
#include <NeoPixelBus.h>
#include <LinkedList.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>
#include <Preferences.h>
#include "debug.h"
#include "timer.h"
#include "button.h"
#include "config.h"
#include "animation.h"
#include "strip.h"
#include "state.h"

SSD1306Wire display(0x3c,5,4);
Preferences perfs;
void drawDebugString(String s)
{
	display.clear();
	display.drawString(0,0,s);
  display.display();
}
bool bBLEClientConnected = false;
BLEServer * pBLEServer = NULL;
BLEService* pBLEService = NULL;
#define SheildSerivceUUID "c272318f-93bc-4e34-8b8a-81e2706f106a"
#define NOW_HIT_UUID "89d6717f-039c-4e6b-b68d-b2468c846057"
#define NOW_TECH_UUID "8c611193-e464-4e12-8dfb-a1982ccf9071"
#define NOW_SHEILD_UUID "15298eda-49b2-4fca-a3e2-f63d8cad817e"

#define FULL_HIT_UUID "148bae7b-9d77-4a23-83cc-2bbf5b003b48"
#define FULL_TECH_UUID "8d5de9ae-b4d4-47f0-a3f0-c8702914a1c7"
#define FULL_SHELD_UUID "5657e13e-215c-4885-872c-ce32031a336c"

BLECharacteristic BC_NowHit(NOW_HIT_UUID,BLECharacteristic::PROPERTY_READ);
BLECharacteristic BC_NowTech(NOW_TECH_UUID,BLECharacteristic::PROPERTY_READ);
BLECharacteristic BC_NowSheild(NOW_SHEILD_UUID,BLECharacteristic::PROPERTY_READ);
BLECharacteristic BC_FullHit(FULL_HIT_UUID,BLECharacteristic::PROPERTY_READ|BLECharacteristic::PROPERTY_WRITE);
BLECharacteristic BC_FullTech(FULL_TECH_UUID,BLECharacteristic::PROPERTY_READ|BLECharacteristic::PROPERTY_WRITE);
BLECharacteristic BC_FullSheild(FULL_SHELD_UUID,BLECharacteristic::PROPERTY_READ|BLECharacteristic::PROPERTY_WRITE);

class  BLECharWrireCallback : public BLECharacteristicCallbacks  {
  public:
  void onRead(BLECharacteristic* pCharacteristic)
  {

  }
	void onWrite(BLECharacteristic* pCharacteristic)
  {
    if(pCharacteristic == &BC_FullHit)
    {
      Status.FullHit = *pCharacteristic -> getData();
      perfs.putUChar("FullHit",Status.FullHit);
    }

    if(pCharacteristic == &BC_FullTech)
    {
      Status.FullTech = *pCharacteristic -> getData();
      perfs.putUChar("FullTech",Status.FullTech);
    }

    if(pCharacteristic == &BC_FullSheild)
    {
      Status.FullSheld = *pCharacteristic -> getData();
      perfs.putUChar("FullShield",Status.FullSheld);
    }
  }
};
BLECharacteristic BLEBatlevel(BLEUUID((uint16_t)0x2A19),BLECharacteristic::PROPERTY_READ|BLECharacteristic::PROPERTY_NOTIFY);
BLEDescriptor BLEBattLevelDescriptor(BLEUUID((uint16_t)0x2901));
class SheildSrvCallback: public BLEServerCallbacks {
  void onConnect(BLEServer* srv)
  {
    bBLEClientConnected = true;
  }
  void onDisconnect(BLEServer* srv)
  {
    bBLEClientConnected = false;
  }
};

void BLEInit()
{
  BLEDevice::init("Shield");
  pBLEServer = BLEDevice::createServer();
  pBLEServer->setCallbacks(new SheildSrvCallback());

  BLEService *psvc  = pBLEServer->createService(SheildSerivceUUID);

  psvc->addCharacteristic(&BC_NowHit);
  psvc->addCharacteristic(&BC_NowTech);
  psvc->addCharacteristic(&BC_NowSheild);

  BLECharWrireCallback* wrclbk = new BLECharWrireCallback();

  BC_FullHit.setCallbacks(wrclbk);
  psvc->addCharacteristic(&BC_FullHit);
  
  BC_FullTech.setCallbacks(wrclbk);
  psvc->addCharacteristic(&BC_FullTech);

  BC_FullSheild.setCallbacks(wrclbk);
  psvc->addCharacteristic(&BC_FullSheild);

  psvc->addCharacteristic(&BLEBatlevel);
  BLEBattLevelDescriptor.setValue("Voltage");
  BLEBatlevel.addDescriptor(&BLEBattLevelDescriptor);
  BLEBatlevel.addDescriptor(new BLE2902());
  psvc->start();

  pBLEServer->getAdvertising()->addServiceUUID(SheildSerivceUUID);
  pBLEServer->getAdvertising()->start();
}


void setup() {
  Serial.begin(115200);
  Serial.println("string");
  perfs.begin("Sheild");
 
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  
  debug("display started");
  strip = new LedStrip(Leds, LedsPin);
	strip->Start();

  debug("display strip inited");

  bHit.Init(25);
	bHeal.Init(26 );

	Status.Init();
  State::setState(&IdleState);
  
  Status.FullHit=perfs.getUChar("FullHit",2);
  Status.FullTech=perfs.getUChar("FullTech",0);
  Status.FullSheld=perfs.getUChar("FullShield",0);
  BC_FullHit.setValue(&Status.FullHit,sizeof(Status.FullHit));
  BC_FullTech.setValue(&Status.FullTech,sizeof(Status.FullTech));
  BC_FullSheild.setValue(&Status.FullSheld,sizeof(Status.FullSheld));
  Status.NowHit=Status.FullHit;
  Status.NowTech=Status.FullTech;
  BLEInit();
  debug("ready");
}

unsigned long lastUpdate=0;
#define UPDATE_TIME 20
void loop() {
  
	if(pStateNow!=0)
		if(millis()-lastUpdate > UPDATE_TIME )
		{
			pStateNow->onUpdate();
			lastUpdate = millis();
       BC_NowHit.setValue(&Status.NowHit,sizeof(Status.NowHit));
       BC_NowTech.setValue(&Status.NowTech,sizeof(Status.NowTech));
       BC_NowSheild.setValue(&Status.NowShield,sizeof(Status.NowShield));
		}
	//if(digitalRead(2)==0)
	//	delay(2000);
	for(uint8_t i=0;i<timers.size();i++)
	{
		Timer *t=timers.get(i);
		t->check();
	}
	if (bHit.check())
		delay(100);
	if(bHeal.check())
		delay(100);

  

 
}