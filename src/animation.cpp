#include <Arduino.h>

#include "config.h"
#include "debug.h"
#include "animation.h"
#include "button.h"
#include "timer.h"
#include "state.h"
int getMin(unsigned short *m, unsigned short len) {
	int ret = 0, i = 0;
	if (m == 0)
		return 0;
	for (i = 0; i < len; i++) {
		while (m[ret] == -1) {
			ret++;
			if (ret > len)
				return -1;
		}
		if (m[i] == -1)
			continue;
		if (m[ret] > m[i]) {
			ret = i;
		}
	}
	return ret;

}
//HitAnimation:
void HitAnimation::Init(t_nbBus *b) {
	debugline();
		numDots = numtDots + 2;

		dots = new sDot[numDots];
		strip = b;
		leds = strip->PixelCount();

	}

void HitAnimation::Start() {
	int i, id, m;
	//Временый склад случайных горячих и холодных точек
	unsigned short _hDots[numtDots];
	debug("Hot and cold values:");
	for (i = 0; i < numtDots; i++) {
		_hDots[i] = random(leds);
	}

	bool hot = random(2);

	for (i = 0; i < numDots; i++, hot = !hot) {

		if (i == 0) {
			dots[i].addr = 0;
			dots[i].rgb = _getRgb(hot);
			continue;
		}
		if (i == (numDots - 1)) {
			dots[i].addr = leds - 2;
			dots[i].rgb = _getRgb(hot);
			continue;
		}

		id = getMin(_hDots, numtDots);
		m = _hDots[id];
		dots[i].addr = m;
		dots[i].rgb = _getRgb(hot);
		_hDots[id] = -1;

	}
	strip->Show();
	startTime=millis();
}
void HitAnimation::End() {

}

bool HitAnimation::Complete()
{
	return ((millis()-startTime)>(HIT_ANIM_TIME*1000));

}

bool HitAnimation::Update(unsigned long time) {
	int i;
	sDot prev = dots[0], next = dots[1];
	unsigned short iDot = 1;
	for (int i = 0; i < leds; i++) {
		if ((i > next.addr) && (iDot < numDots)) {
			prev = next;
			next = dots[iDot];
			iDot++;
		}
		if (i == prev.addr) {
			strip->SetPixelColor(prev.addr, prev.rgb);
			//strip->SetPixelColor(prev.addr,RgbColor(255,0,0));
			continue;
		}
		if (i == next.addr) {
			strip->SetPixelColor(prev.addr, prev.rgb);
			//strip->SetPixelColor(prev.addr,RgbColor(0,255,0));
			continue;
		}
		float inorm = i - prev.addr;
		float pos = inorm / (next.addr - prev.addr);
		strip->SetPixelColor(i, RgbColor::LinearBlend(prev.rgb, next.rgb, pos));
	}
	for (i = 1; i < numDots - 1; i++) {
		if ((dots[i].addr < 0) || dots[i].addr > leds)
			dots[i].addr = random(leds);
		int a = random(10);
		if (a < 1) {
			dots[i].addr += 20;
			continue;
		}
		if (a > 8) {
			dots[i].addr -= 20;
			continue;
		}
		if (a < 4) {
			dots[i].addr++;
			continue;
		}
		if (a > 6) {
			dots[i].addr--;
			continue;
		}

	}
	return true;
}
RgbColor HitAnimation::_getRgb(bool hot) {
		int cold;
		if(Status.NowShield>1)
			cold=1;
		else
			cold=0;
		if (hot)
			return RgbColor(cold, 0, (Status.NowShield+1)*STRIP_IDLE_BRIGHTNESS*2);
		else
			return RgbColor(cold, 0, cold);
	}

//IdleAnimation
void IdleAnimation::Init(t_nbBus* b) {
	debugline();
	strip = b;

}
void IdleAnimation::Start() {
	debugline();
	strip->ClearTo(RgbColor(0, 0, Status.NowShield*STRIP_IDLE_BRIGHTNESS));
	strip->Show();
}
bool IdleAnimation::Complete()
{
	return false;
}
void IdleAnimation::End() {

}
bool IdleAnimation::Update(unsigned long time) {

	return false;
}

//IdleAnimation
void BleedingAnimation::Init(t_nbBus* b) {
	debugline();
	strip = b;


}
void BleedingAnimation::Start() {
	debugline();
	bEven=false;
}
bool BleedingAnimation::Complete()
{
	return false;
}
void BleedingAnimation::End() {

}
bool BleedingAnimation::Update(unsigned long time) {
	unsigned long now=millis();
	if(now-lastUpdate<1000)
		return false;
	lastUpdate=now;
	for(uint16_t i=0;i<strip->PixelCount();i++)
	{
		static int br=STRIP_IDLE_BRIGHTNESS*3;
		if(i%2==bEven)
			strip->SetPixelColor(i,RgbColor(br,br,0));
		else
			strip->SetPixelColor(i,RgbColor(0,0,Status.NowShield*STRIP_IDLE_BRIGHTNESS));
	}
	bEven=!bEven;
	return true;
}
void WoundedAnimation::Init(t_nbBus* b) {
	debugline();
	strip = b;


}
void WoundedAnimation::Start() {
	debugline();
}
bool WoundedAnimation::Complete()
{
	return false;
}
void WoundedAnimation::End() {

}
bool WoundedAnimation::Update(unsigned long time) {
	if(Status.NowShield==last_sheld_state)
		return false;

		for(uint16_t i=0;i<strip->PixelCount();i++)
		{
			static int br=STRIP_IDLE_BRIGHTNESS*3;
			if(i%2)
				strip->SetPixelColor(i,RgbColor(br,br,0));
			else
				strip->SetPixelColor(i,RgbColor(0,0,Status.NowShield*STRIP_IDLE_BRIGHTNESS));
		}
		return true;
}
void DeadAnimation::Init(t_nbBus* b) {
	debugline();
	strip = b;


}
void DeadAnimation::Start() {
	debugline();
	bEven=false;
}
bool DeadAnimation::Complete()
{
	return false;
}
void DeadAnimation::End() {

}
bool DeadAnimation::Update(unsigned long time) {
	unsigned long now=millis();
	if(now-lastUpdate<1000)
		return false;
	lastUpdate=now;
	for(uint16_t i=0;i<strip->PixelCount();i++)
	{
		static int br=STRIP_IDLE_BRIGHTNESS*3;
		if(i%2==bEven)
			strip->SetPixelColor(i,RgbColor(br,0,0));
		else
			strip->SetPixelColor(i,RgbColor(0,0,0));
	}
	bEven=!bEven;
	return true;
}

void RechargeAnimation::Init(t_nbBus* b) {
	debugline();
	strip = b;
	leds = strip->PixelCount();
}
void RechargeAnimation::Start() {
	debugline();

	now=0;
	strip->ClearTo(RgbColor(0, 0, 0));
	strip->SetPixelColor(0,RgbColor(0,0,Status.NowShield*STRIP_IDLE_BRIGHTNESS));
	strip->SetPixelColor(1,RgbColor(0,0,255));
	strip->Show();
}
void RechargeAnimation::End() {

}
bool RechargeAnimation::Complete()
{
	return (now>=leds);
}
bool RechargeAnimation::Update(unsigned long time) {
	if((millis()-lastUpdate) < 50)
				return false;
	strip->ShiftRight(1);
	now++;
	return true;
}

void ChargeAnimation::Init(t_nbBus* b) {
	debugline();
	strip = b;
	leds = strip->PixelCount();
}
void ChargeAnimation::Start() {
	debugline();
	
}
void ChargeAnimation::End() {

}
bool ChargeAnimation::Complete()
{
	return false;
}
bool ChargeAnimation::Update(unsigned long time) {
	if((millis()-lastUpdate) < 50)
				return false;

	for(int i=0;i<leds;i++)
	{
		if(i%2==even)
			strip->SetPixelColor(i,RgbColor(192,192,192));
		else
			strip->SetPixelColor(i,RgbColor(0,0,0));

	}
	even=!even;
	strip->Show();

	return true;
}

void StandbyAnimation::Init(t_nbBus* b) {
	debugline();
	strip = b;
	leds = strip->PixelCount();
}
void StandbyAnimation::Start() {
	debugline();
	strip->ClearTo(RgbColor(0,0,0));
	strip->Show();
	
}
void StandbyAnimation::End() {

}
bool StandbyAnimation::Complete()
{
	return false;
}
bool StandbyAnimation::Update(unsigned long time) {
	
	return false;
}

