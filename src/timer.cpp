#include <Arduino.h>
#include "debug.h" 
#include "timer.h"
LinkedList<Timer*> timers;
//Timer
void Timer::set(unsigned long s) {
	unsigned long now = millis();

	if (s == 0)
		return;
	end = now + s;
	debug("Set timer to ");
	debug(s);
	debug(" now=");
	debugln(now);
}

bool Timer::check() {
	if (end == 0)
		return false;
	if (millis() >= end) {
		action();
		end = 0;
		return true;
	}
	return false;
}
