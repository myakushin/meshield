//Класс для запуска отложенных событий
#include <LinkedList.h>
class Timer
{
public:
	unsigned long end;
	virtual ~Timer()
	{

	}
	//Установить время срабатывния через s мс от текущего
	void set(unsigned long s);
	//Класс виртуальный, поэтому реальное действие надо описывать в наследнике этого клсса
	//через реализацию этой функции
	virtual void action()=0;
	//Не настало ли время. Возвращает true если таки настало.
	bool check();

};

extern LinkedList<Timer*> timers;