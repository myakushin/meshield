//Виртуальный класс для описанрия действий по кнопкам
class ButtonAction {
public:
	virtual void press(int holdtime)=0;
	virtual ~ButtonAction() {

	}
};
//Тестовые действия. Включает и выключает 13ый выход

//Логика кнопки
class Button {
public:
	int pin;
	bool pressed = false;
	unsigned long pressTime = 0;
	unsigned long holdTime = 0;
	unsigned long releaseTime = 0;
	ButtonAction* pressAct = 0, *holdAct = 0;
	bool holdPlayed = false;

	Button();
	//Задается что делать при коротком и длинном нажатии
	void SetPress(ButtonAction* ba);
	void SetHold(ButtonAction* ba);
	void Init(int p);
	//Проверка а не нажата ли конпка и отрабатывание соответвующего действия.
	bool check();
} ;

extern Button bHit, bHeal;

