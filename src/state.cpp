#include <Arduino.h>
#include <SSD1306.h>
#include "config.h"

#include "debug.h"
#include "button.h"
#include "timer.h"
#include "state.h"
#include "animation.h"
#include "strip.h"

_Status Status;
_IdleState IdleState;
_BleedingState BleedingState;
_WoundedState WoundedState;
_DeadState DeadState;
_ChargingState ChargingState;
State *pStateNow;

_ButtonAction_Nothing ButtonAction_Nothing;
_RechargeTimer RechargeTimer;
_ChargeTimer ChargeTimer;
_StandbyTimer StandbyTimer;
_ButtonAction_Medgel ButtonAction_Medgel;
_ButtonAction_Panacilin ButtonAction_Panacilin;
_ButtonAction_Hit ButtonAction_Hit;
_ButtonAction_Charge ButtonAction_Charge;
_ButtonAction_EndCharge ButtonAction_EndCharge;
extern SSD1306Wire display;
extern bool bBLEClientConnected;
void State::setState(State* n)
{
	debugline();
	if(pStateNow!=0)
		pStateNow->exit();
	pStateNow=n;
	pStateNow->enter();
	debugline();
}

void State::defaultUpdate()
{
	strip->update();
	if((strip->paNow!=0)&&(strip->paNow->Complete()))
	{
		if(Status.NowHit>0)
			strip->SetAnimation(IdleAnimation::ANIM_NUM);
		else
			strip->SetAnimation(BleedingAnimation::ANIM_NUM);
				
	}
	Status.updateScreen();
}


void _IdleState::enter() {
	debugln("Enter idle state");
	strip->SetAnimation(IdleAnimation::ANIM_NUM);
	
	bHit.SetPress(&ButtonAction_Hit);
	bHit.SetHold(&ButtonAction_Charge);
	bHeal.SetPress(&ButtonAction_Panacilin);
	StandbyTimer.set(IDLE_TIMEOUT*1000);
}

void _ButtonAction_Charge::press(int h)
{
	State::setState(&ChargingState);
}
void _ButtonAction_EndCharge::press(int h)
{
	State::setState(&IdleState);
}
void _IdleState::hit()
{
	debugln("Set hit animation");
	strip->SetAnimation(HitAnimation::ANIM_NUM);
	StandbyTimer.set(IDLE_TIMEOUT*1000);

}
void _IdleState::onUpdate()
{
	defaultUpdate();
	if(RechargeTimer.end == 0 && (Status.NowShield < Status.FullSheld ))
		RechargeTimer.set(SHIELD_RECHARIGE_TIME*1000);
	
	if(Status.NowShield==Status.FullSheld)
	{
		if(StandbyTimer.end == 0)
		{
			strip->SetAnimation(StandbyAnimation::ANIM_NUM);
			StandbyTimer.set(1000);
		}

	}
	



}
void _IdleState::exit()
{
	debugln("exit idle state");
}
void _ChargingState::enter()
{
	bHit.SetPress(&ButtonAction_EndCharge);
	strip->SetAnimation(ChargeAnimation::ANIM_NUM);
	RechargeTimer.end =0;
	ChargeTimer.set(10*1000);
}
void _ChargingState::exit()
{

}
void _ChargingState::onUpdate()
{
	strip->update();
	Status.updateScreen();

}
void _BleedingState::enter()
{
	bHit.SetPress(&ButtonAction_Nothing);
	bHit.SetHold(&ButtonAction_Nothing);
	strip->SetAnimation(BleedingAnimation::ANIM_NUM);
	bHeal.SetPress(&ButtonAction_Medgel);
	bHeal.SetHold(&ButtonAction_Panacilin);
	startTime=millis();
}
void _BleedingState::exit()
{

}
void _BleedingState::onUpdate()
{
	defaultUpdate();
	if((millis()-startTime)>BLEEDING_TIME)
	{
		debugln("YOU DEAD!");
		State::setState(&DeadState);
	}
}
void _WoundedState::enter()
{
	bHit.SetPress(&ButtonAction_Nothing);
	bHit.SetHold(&ButtonAction_Nothing);
	strip->SetAnimation(WoundedAnimation::ANIM_NUM);

}

void _WoundedState::exit()
{

}
void _WoundedState::onUpdate()
{
	defaultUpdate();
}

void _DeadState::enter() {
	bHit.SetPress(&ButtonAction_Nothing);
	bHit.SetHold(&ButtonAction_Nothing);
	bHeal.SetPress(&ButtonAction_Nothing);
	bHeal.SetHold(&ButtonAction_Nothing);

	strip->SetAnimation(DeadAnimation::ANIM_NUM);
	Status.updateScreen();

}
void _DeadState::exit() {

}
void _DeadState::onUpdate() {
	defaultUpdate();
}

void _StandbyTimer::action()
{
	
}
//_Status

int drawPoints(int startPos,int cur,int max,char type)
{
	int pos=startPos;
	const int posoffset=10;
	int i;
	for(i=0;i<cur ;i++,pos+=posoffset)
		display.fillCircle(pos,30,4);
	for(;i<max ;i++,pos+=posoffset)
		display.drawCircle(pos,30,4);
	
	display.drawString(startPos-5,10,String(type));
	return pos;
}
void _Status::updateScreen() {
	display.clear();
	if(pStateNow!=0)
	    display.drawString(0,0,pStateNow->name+String(" ")+String((int)bBLEClientConnected)+String(" "));
	
	int pos=5;
	pos=drawPoints(pos,NowHit,FullHit,'B');
	pos=drawPoints(pos,NowTech,FullTech,'T');
	pos=drawPoints(pos,NowShield,FullSheld,'S');
	if(pStateNow==&ChargingState)
	{
		unsigned int toend = ChargeTimer.end - millis();
		display.drawString(0,50,"***Charge time "+String(toend)+" ms ****");
	}
	else
	{  	if(RechargeTimer.end!=0)
 	{
		unsigned int toend = RechargeTimer.end - millis();
		display.drawString(0,50,"***Recharing in "+String(toend)+" ms ****");
	}
	else
	{
		display.drawString(0,50,"Ready");
	}
	}
	display.display();
}
void _Status::hit() {
	if (NowShield > 0) {
		NowShield--;
	} else	 {
		if(NowTech > 0)
					NowTech--;
			else
					NowHit--;
	}
	if(NowHit<1)
		State::setState(&BleedingState);
	RechargeTimer.set(SHIELD_RECHARIGE_TIME*1000);
	updateScreen();
}
void _Status::panacilin()
{
	if((pStateNow==&WoundedState)||(pStateNow==&BleedingState))
		State::setState(&IdleState);
	if(NowHit<FullHit)
		NowHit++;
	updateScreen();
} 

void _Status::Init() {
	NowShield = 0;
	NowHit = FullHit;
	updateScreen();
	timers.add(&RechargeTimer);
	timers.add(&ChargeTimer);
	timers.add(&StandbyTimer);
	RechargeTimer.set(SHIELD_RECHARIGE_TIME*1000);
}

void _ButtonAction_Hit::press(int h) {
	debugln(F("Hit button pressed"));
	int s=Status.NowShield;//Запомним состояние щита чтобы понимать, надо ли отигрывать анимацию
	Status.hit();
	if(s>0)
		IdleState.hit();

}
void _RechargeTimer::action()
{
	if(Status.NowShield < Status.FullSheld )
		Status.NowShield++;
	
	debugline();
	Status.updateScreen();
	debugline();
	strip->SetAnimation(RechargeAnimation::ANIM_NUM);

}

void _ChargeTimer::action()
{
	State::setState(&IdleState);
}
void _ButtonAction_Medgel::press(int h)
{
	State::setState(&WoundedState);
}
void _ButtonAction_Panacilin::press(int h)
{
	Status.panacilin();
}