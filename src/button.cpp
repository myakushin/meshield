#include <Arduino.h>
#include "button.h"
#include "config.h"

Button::Button() {
	pin = 0;
}

void Button::SetPress(ButtonAction* ba) {
	if (ba == 0)
		return;
	pressAct = ba;
}
void Button::SetHold(ButtonAction* ba) {
	if (ba == 0)
		return;
	holdAct = ba;
}

void Button::Init(int p) {
	pin = p;
	pinMode(pin, INPUT_PULLUP);
	pressAct = 0;
	holdAct = 0;
}

bool Button::check() {
	unsigned long now = millis();
	bool prev = pressed;

	//Игнорируем нажатия короче чем DEBOUNCE_TIME
	if ((now - pressTime < DEBOUNCE_TIME)
			|| (now - releaseTime < DEBOUNCE_TIME))
		return pressed;

	pressed = (digitalRead(pin) == 0);

	if (pressed) {

		if (!prev) {
			pressTime = now;
		} else
			holdTime = now - pressTime;
		if (holdTime > 5000) {
			if (!holdPlayed)
				holdAct->press(holdTime);
			holdPlayed = true;
		} else
			holdPlayed = false;

	} else {
		//Кнопка была отпущена
		if (prev) {
			releaseTime = now;
			if (!holdPlayed) {
				pressAct->press(holdTime);
			}
		}
	}
	return pressed;
}

Button bHit, bHeal;


