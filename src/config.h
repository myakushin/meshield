#ifndef SHIELD_CONFIG_H
#define SHIELD_CONFIG_H
#include <NeoPixelBus.h>
//Констатны. Возможно редактирования для адаптации под железо

const int Leds = 20; //Количество светодиодов в ленте
const int LedsPin = 16; //Пины к которым подключена лента
const int HIT_ANIM_TIME = 2; //Время отигрыша анимации попадания
const int STRIP_IDLE_BRIGHTNESS=16; //Стадартная яркость щита на хит.
const int DEBOUNCE_TIME = 50; //(мс)Минимальное время переключение кнопки. Нужно для борьбы с дребезгом контаков
const int ANIMS_MAX = 8;
const int SHIELD_RECHARIGE_TIME=300; //Время перезарядки щита в секундах
const int INDICATOR_PIN=0;
const int INDICATOR_LEN=9;
const int IDLE_TIMEOUT=900;
//Свои типы. Чтобы не писать много раз
typedef NeoPixelBus<NeoRgbFeature, Neo800KbpsMethod> t_nbBus;

#endif