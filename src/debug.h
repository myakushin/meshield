#ifndef SHIELD_DEBUG_H
#define SHIELD_DEBUG_H

#define DEBUG 1
#ifdef DEBUG
#define debug(x) Serial.print(x)
#define debugln(x) Serial.println(x);delay(100);
#else
#define debug(x)
#define debugln(x)
#endif

#define LINEDEBUG 1
#ifdef LINEDEBUG
#define debugline() 
#endif
#endif