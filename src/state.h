#ifndef SHIELD_STATE_H
#define SHIELD_STATE_H
//Базовый класс для логики конечного автомата.
//Описывает текущее состояние щита
class State
{
public:
	String name;
	virtual void enter()=0;
	virtual void exit()=0;
	virtual void onUpdate()=0;
	void defaultUpdate();
	virtual ~State()
	{

	}
	//Общий метод, переключающий состояния
	static void setState(State* n);
};

//Обычное состояние щита
class _IdleState:public State
{
public:
	_IdleState() {
		name = String("Idle");
	}
	virtual void enter();
	virtual void exit();
	virtual void onUpdate();

	void hit();
};
class _ChargingState:public State
{
public:
	_ChargingState() {
		name = String("!CHAAARGE!");
	}
	static const uint32_t CHARGE_TIME=1*60*1000;
	virtual void enter();
	virtual void exit();
	virtual void onUpdate();
};
class _BleedingState:public State
{
private:
	unsigned long startTime;
public:
	_BleedingState() {
		name = String("Bleeding");
	}
	static const uint32_t BLEEDING_TIME=1*60*1000;
	virtual void enter();
	virtual void exit();
	virtual void onUpdate();
};
class _WoundedState:public State
{
public:
	_WoundedState(){
		name = String("Wounded");
	}
	virtual void enter();
	virtual void exit();
	virtual void onUpdate();
};
class _DeadState:public State
{
public:
	_DeadState() {
		name = String("Dead");
	}
	virtual void enter();
	virtual void exit();
	virtual void onUpdate();
};
//Текущее состояние щита в цифрах


class _Status
{
public:
	unsigned char NowShield=0,FullSheld=2;
	unsigned char NowTech=0,FullTech=2;
	unsigned char NowHit=0,FullHit=2;
	void updateScreen();
	void hit();
	void panacilin();
	//Обработчик кнопки

	void Init();

};

extern _Status Status;
extern _IdleState IdleState;
extern _BleedingState BleedingState;
extern _WoundedState WoundedState;
extern _DeadState DeadState;
extern State *pStateNow;

class _ButtonAction_Nothing: public ButtonAction {
	virtual void press(int h) {
	}
};

class _ButtonAction_Medgel: public ButtonAction {
	virtual void press(int h);
};


class _ButtonAction_Panacilin: public ButtonAction {
	virtual void press(int h);
};


class _ButtonAction_Hit: public ButtonAction {
	virtual void press(int h);

};
class _ButtonAction_Charge: public ButtonAction {
	virtual void press(int h);

};
class _ButtonAction_EndCharge: public ButtonAction {
	virtual void press(int h);

};
class _RechargeTimer: public Timer
{
public:
	virtual void action();
};
class _ChargeTimer: public Timer
{
public:
	virtual void action();
};
class _StandbyTimer: public Timer
{
public:
	virtual void action();
};

extern _ButtonAction_Nothing ButtonAction_Nothing;
extern _RechargeTimer RechargeTimer;
extern _ButtonAction_Medgel ButtonAction_Medgel;
extern _ButtonAction_Panacilin ButtonAction_Panacilin;
extern _ButtonAction_Hit ButtonAction_Hit;
#endif